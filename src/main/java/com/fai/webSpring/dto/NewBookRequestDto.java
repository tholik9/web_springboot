package com.fai.webSpring.dto;

import javax.validation.constraints.NotBlank;

/**
 *
 * @author th
 */
public class NewBookRequestDto {

	@NotBlank
	private String title;
	@NotBlank
	private String description;
	@NotBlank
	private String author;
	@NotBlank
	private String isbn;
	private Integer genreId;

	public NewBookRequestDto() {
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getAuthor() {
		return author;
	}

	public String getISBN() {
		return isbn;
	}

	public int getGenreId() {
		return genreId;
	}

}
