package com.fai.webSpring.dto;

/**
 *
 * @author th
 */
public class EditBookRequestDto extends NewBookRequestDto {

	private Long id;

	public EditBookRequestDto() {
	}

	public Long getId() {
		return id;
	}

}
