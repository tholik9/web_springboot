package com.fai.webSpring.dto;

import com.fai.webSpring.enums.GenreType;

/**
 *
 * @author th
 */
public class GenreResponseDto {

	private int id;
	private String nameCz;

	public GenreResponseDto(GenreType genre) {
		this.id = genre.ordinal();
		this.nameCz = genre.getNameCz();
	}

	public int getId() {
		return id;
	}

	public String getNameCz() {
		return nameCz;
	}
}
