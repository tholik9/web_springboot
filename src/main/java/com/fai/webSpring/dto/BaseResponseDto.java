package com.fai.webSpring.dto;

/**
 *
 * @author th
 */
public class BaseResponseDto {
	private String message;

	public BaseResponseDto() {
	}

	public BaseResponseDto(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
	
}
