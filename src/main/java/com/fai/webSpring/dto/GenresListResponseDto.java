package com.fai.webSpring.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author th
 */
public class GenresListResponseDto {

	private List<GenreResponseDto> genreType;

	public GenresListResponseDto() {
		genreType = new ArrayList<GenreResponseDto>();
	}

	public GenresListResponseDto(List<GenreResponseDto> genreType) {
		this.genreType = genreType;
	}

	public List<GenreResponseDto> getGenreType() {
		return genreType;
	}

	public void addGenre(GenreResponseDto genre) {
		genreType.add(genre);
	}}

