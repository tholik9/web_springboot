package com.fai.webSpring.dto;

public class JwtAuthenticationResposeDto {
    private String accessToken;
    private String tokenType = "Bearer";

    public JwtAuthenticationResposeDto(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }
}