package com.fai.webSpring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 *
 * @author th
 */
@Configuration
@EnableJpaAuditing
public class AuditingConfig {
	
}
