package com.fai.webSpring.rest;

import com.fai.webSpring.dto.BaseResponseDto;
import com.fai.webSpring.dto.EditBookRequestDto;
import com.fai.webSpring.dto.GenreResponseDto;
import com.fai.webSpring.dto.GenresListResponseDto;
import com.fai.webSpring.dto.NewBookRequestDto;
import com.fai.webSpring.enums.GenreType;
import com.fai.webSpring.model.Book;
import com.fai.webSpring.repository.BookRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
@RequestMapping("/api/v1/book")
public class BookResource {

	@Autowired
	private BookRepository bookRepository;

	@PostMapping("/new")
	public ResponseEntity<?> newBook(@Valid @RequestBody NewBookRequestDto newBookRequest) {

		final GenreType genre = GenreType.getById(newBookRequest.getGenreId());
		Book book = new Book(newBookRequest.getTitle(),
				newBookRequest.getDescription(),
				newBookRequest.getAuthor(),
				newBookRequest.getISBN(),
				genre);
		bookRepository.save(book);
		return ResponseEntity.ok(new BaseResponseDto("Nová kniha byla úspěšně přídána."));
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> editBook(@Valid @RequestBody EditBookRequestDto editBookRequest) {

		Optional<Book> bookOp = bookRepository.findById(editBookRequest.getId());
		if (bookOp.isPresent()) {
			Book book = bookOp.get();
			book.setAuthor(editBookRequest.getAuthor());
			book.setISBN(editBookRequest.getISBN());
			book.setTitle(editBookRequest.getTitle());
			book.setDescription(editBookRequest.getDescription());
			GenreType genre = GenreType.getById(editBookRequest.getGenreId());
			book.setGenre(genre);
			bookRepository.save(book);
			return ResponseEntity.ok(new BaseResponseDto("Kniha byla uspěšně uložena."));
		}
		BaseResponseDto response = new BaseResponseDto("Kniha nebyla nalezena. Neplatné id.");
		return ResponseEntity.badRequest().body(response);
	}

	@RequestMapping(
			value = "/list",
			params = {"page", "size"},
			method = RequestMethod.GET
	)
	public Page<Book> list(
			@RequestParam("page") int page, @RequestParam("size") int size) {
		Page<Book> resultPage = bookRepository.findAll(new PageRequest(page - 1, size));
//        if (page > resultPage.getTotalPages()) {
//            throw new MyResourceNotFoundException();
//        }

		return resultPage;
	}

	@RequestMapping(value = "/id/{bookId}", method = GET)
	@ResponseBody
	public ResponseEntity<?> bookById(@PathVariable Long bookId) {
		Optional<Book> book = bookRepository.findById(bookId);
//        if (page > resultPage.getTotalPages()) {
//            throw new MyResourceNotFoundException();
//        }
		if (!book.isPresent()) {
			BaseResponseDto response = new BaseResponseDto("Kniha nebyla nalezena. Neplatné id.");
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(book.get());
	}

	@GetMapping("/genres")
	public ResponseEntity<?> genresList() {

		List<GenreResponseDto> list = Stream.of(GenreType.values())
				.map(GenreResponseDto::new)
				.collect(Collectors.toList());
		GenresListResponseDto genresList = new GenresListResponseDto(list);
		return ResponseEntity.ok(genresList);
	}
}
