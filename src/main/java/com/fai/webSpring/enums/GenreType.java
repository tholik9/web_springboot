package com.fai.webSpring.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 *
 * @author th
 */
@JsonFormat(shape= JsonFormat.Shape.OBJECT)
public enum GenreType {
	ARCHITECTURE("Architektura"),
	BIBLIOGRAPHY_MEMOARS("Biografie a memoáry"),
	CRIME("Detektivky"),
	THEATRE_PLAY("Divadelní hry"),
	GIRLY_FICTION("Dívčí romány"),
	ADVENTURE("Dobrodružné"),
	TRANSPORTATION("Doprava"),
	SPIRITUAL("Duchovní literatura"),
	HOUSING("Dům a byt"),
	ECOLOGY("Ekologie, živ. prostředí"),
	ECONOMY("Ekonomie a obchod"),
	EDUCATION("Encyklopedie, vzdělání"),
	EROTIC("Erotika"),
	ESOTERIC("Esoterika, astrologie, okultismus"),
	FATNASY("Fantasy"),
	ESSAY("Fejetony, eseje"),
	PHILOSOPHY("Filozofie"),
	HISTORY("Historie"),
	HOBBIES("Hobby"),
	HORRORS("Horory"),
	HUMOR("Humor"),
	LANGUAGES("Jazyky, lingvistika"),
	COMICS("Komiksy"),
	COOKING("Kuchařky"),
	MAPS("Mapy a atlasy"),
	MATH("Matematika a logika"),
	MYTHOLOGY("Mytologie"),
	RELIGION("Náboženství"),
	NEW_AGE("New Age"),
	SELF_DEVELOPMENT("Osobní rozvoj a styl"),
	PC("PC literatura"),
	POETRY("Poezie"),
	FAIRY_TALE("Pohádky"),
	POLITICS("Politologie, mezinárodní vztahy"),
	FABLE("Pověsti"),
	TALE("Povídky"),
	NATURE("Příroda, zvířata"),
	NATURE_SCIENCE("Přírodní vědy"),
	PSYCHOLOGY("Psychologie a pedagogika"),
	FAMILY("Rodina"),
	FICTION("Romány"),
	SCIFI("Sci-fi"),
	SOCIOLOGY("Sociologie"),
	SPORT("Sport"),
	TECH("Technika a elektro"),
	THRILLER("Thrillery"),
	ART("Umění"),
	WAR("Válečné"),
	SCIENCE("Věda"),
	COSMOS("Vesmír"),
	MYSTERIS("Záhady"),
	GARDEN("Zahrada"),
	HEALTH("Zdraví"),
	HEALTHCARE("Zdravotnictví");

	private int id;
	private String nameCz;

	private GenreType(String nameCz) {
		this.nameCz = nameCz;
	}

	public String getNameCz() {
		return nameCz;
	}

	public int getId() {
		return this.ordinal();
	}
	
//	@JsonValue
	final String value() {
		return this.nameCz;
	}

	public static GenreType getById(final int id) {
		for (GenreType genre : values()) {
			if (genre.ordinal() == id) {
				return genre;
			}
		}
		return null;
	}
}
