package com.fai.webSpring.enums;

/**
 *
 * @author th
 */
public enum RoleType {
	USER,
	ADMIN
}
