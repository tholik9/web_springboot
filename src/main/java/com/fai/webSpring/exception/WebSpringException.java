package com.fai.webSpring.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class WebSpringException extends RuntimeException {
    public WebSpringException(String message) {
        super(message);
    }

    public WebSpringException(String message, Throwable cause) {
        super(message, cause);
    }
}