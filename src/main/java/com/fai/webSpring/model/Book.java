package com.fai.webSpring.model;

import com.fai.webSpring.enums.GenreType;
import com.fai.webSpring.model.audit.DateAudit;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.NaturalId;

/**
 *
 * @author th
 */
@Entity
@Table(name = "book")
public class Book extends DateAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Enumerated(EnumType.ORDINAL)
	@Column(length = 20)
	private GenreType genre;
	@Column(length = 200)
	private String title;
	@Column(length = 2000)
	private String description;
	@Column(length = 2000)
	private String author;
	private LocalDate publishDate;
	@Column(length = 100)
	private String ISBN;

	public Book() {
	}

	public Book(String title, String description, String author, String ISBN, GenreType genre) {
		this.genre = genre;
		this.title = title;
		this.description = description;
		this.author = author;
		this.publishDate = publishDate;
		this.ISBN = ISBN;
	}

	public Long getId() {
		return id;
	}

	public GenreType getGenre() {
		return genre;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getAuthor() {
		return author;
	}

	public LocalDate getPublishDate() {
		return publishDate;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setGenre(GenreType genre) {
		this.genre = genre;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setPublishDate(LocalDate publishDate) {
		this.publishDate = publishDate;
	}

	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}

	
	
}
