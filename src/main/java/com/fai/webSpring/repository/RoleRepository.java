package com.fai.webSpring.repository;

import com.fai.webSpring.enums.RoleType;
import com.fai.webSpring.model.Role;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author th
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	Optional<Role> findByType(RoleType roleType);
}
