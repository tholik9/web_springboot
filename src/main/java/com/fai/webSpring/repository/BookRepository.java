package com.fai.webSpring.repository;

import com.fai.webSpring.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author th
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
