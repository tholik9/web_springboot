(function () {
	'use strict';

	angular
			.module('app')
			.factory('AuthService', Service);

	function Service($http, $localStorage, $rootScope) {
		var service = {};

		service.Login = Login;
		service.Logout = Logout;
		service.Register = Register;

		return service;

		function Login(email, password, callback) {
			console.log("login auth service");
			if ($localStorage.currentUser) {
				$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
			}
			var userData = new Object();
			userData.usernameOrEmail = email;
			userData.password = password;

			$http({
				method: 'POST',
				url: '/api/v1/auth/signin',
				data: userData
			}).then(function successCallback(response) {
				console.log("login cajk");
				console.log(response);

				// store username and token in local storage to keep user logged in between page refreshes
				$localStorage.currentUser = {usernameOrEmail: userData.usernameOrEmail, token: response.data.accessToken};
				console.log($localStorage.currentUser);
				// add jwt token to auth header for all requests made by the $http service
				$http.defaults.headers.common.Authorization = 'Bearer ' + response.token;
				$rootScope.logged = true;
				console.log($rootScope.logged);
				callback(true);
			}, function errorCallback(response) {
				callback(response.data.message);
			});
		}
		function Register(firstName, lastName, username, email, password, callback) {
			console.log("auth register");
			var userData = new Object();
			userData.firstName = firstName;
			userData.lastName = lastName;
			userData.username = username;
			userData.email = email;
			userData.password = password;
			console.log(userData);
			
			$http({
				method: 'POST',
				url: '/api/v1/auth/signup',
				data: userData
			}).then(function successCallback(response) {
				callback(true);
			}, function errorCallback(response) {
				console.log(response);
				callback(response.data.message);
			});
		}

		function Logout() {
			delete $localStorage.currentUser;
			$rootScope.logged = false;
			$http.defaults.headers.common.Authorization = '';
		}
	}
})();