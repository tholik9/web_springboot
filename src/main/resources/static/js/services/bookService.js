(function () {
	'use strict';

	angular
			.module('app')
			.factory('BookService', Service);

	function Service($http, $localStorage) {
		var service = {};

		service.GetById = GetById;

		return service;

		function GetById(bookId, callback) {
			console.log("book by id");
			if ($localStorage.currentUser) {
				$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
			}

			return $http.get('/api/v1/book/id/' + bookId).then(function (response) {
				return response.data;
			});
		}
	}
})();