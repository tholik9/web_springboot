angular.module('app', [
	'ui.router', 'ngStorage', 'ui.grid', 'ui.grid.pagination'
])
		.config(function ($stateProvider, $urlRouterProvider) {

			$urlRouterProvider.otherwise('/home');
			$stateProvider
					.state('root', {
						abstract: true,
						template: '<div ui-view></div>',
						resolve: {objectX: function () {
								return {x: 'x', y: 'y'};
							}},
						controller: 'rootController',
					})
					.state('home', {
//						parent: "root",
						name: "home",
						url: '/home',
//						templateUrl: 'view.html',
						template: '<h3>Home page</h3>'
					})
					.state('books', {
						name: "books",
						url: '/books',
						templateUrl: 'views/books.html',
						controller: 'booksController',
					})
					.state('bookDetail', {
//						name: "books",
						url: '/:id',
						templateUrl: 'views/bookDetail.html',
						controller: 'bookController',
						resolve: {
							bookDetail: function ($stateParams, BookService) {
								return BookService.GetById($stateParams.id);
							},
							bookEdit: function () {
								return;
							}
						},
					})
					.state('bookEdit', {
						url: '/:id',
						templateUrl: 'views/editBook.html',
						controller: 'bookController',
						resolve: {
							bookEdit: function ($stateParams, BookService) {
								return BookService.GetById($stateParams.id);
							},
							bookDetail: function () {
								return;
							}
						}
					})
					.state('new-book', {
						name: "new-book",
						url: '/new-book',
						templateUrl: 'views/newBook.html',
						controller: 'bookController',
						resolve: {
							bookEdit: function () {
								return;
							},
							bookDetail: function () {
								return;
							}
						}
					})
					.state('register', {
						name: "register",
						url: '/register',
						templateUrl: 'views/register.html',
					})
		})
		.controller('home', function ($scope, $rootScope, AuthService) {
			//init vars
			init();
			function init() {
				$rootScope.errorShow = false;
				$rootScope.errorMsg = false;
				$rootScope.successShow = false;
				$rootScope.successMsg = false;
				$scope.newUser = {};
				$scope.loginUser = {};
				$rootScope.logged = false;
			}

			$scope.login = function () {
				AuthService.Login($scope.loginUser.email, $scope.loginUser.password, function (result) {
					console.log($rootScope.logged);
					if (result === false) {
						$rootScope.errorShow = true;
						$rootScope.errorMsg = "Neplatné přihlašovací údaje.";
					}

				});
			}

			$scope.logout = function () {
				AuthService.Logout();
			}

			$scope.register = function () {
				console.log("register");
				var firstName = $scope.newUser.firstName;
				var lastName = $scope.newUser.lastName;
				var username = $scope.newUser.username;
				var email = $scope.newUser.email;
				var password = $scope.newUser.password;
				console.log(lastName);
				AuthService.Register(firstName, lastName, username, email, password, function (result) {
					if (result === true) {
						$rootScope.successShow = true;
						$rootScope.successMsg = "Byl jste úspěšně registrován.";
					} else {
						$rootScope.errorShow = true;
						$rootScope.errorMsg = result;
					}
				});
			}

			$scope.changeOfRoutes = function () {
				$rootScope.errorShow = false;
				$rootScope.successShow = false;
			}

		})
		.controller('bookDetailController', function ($scope, bookDetail) {
			console.log("SET book detail");
			$scope.bookDetail = bookDetail;
			console.log(bookDetail);
		})
		.controller('bookEditController', function ($scope, bookEdit) {
			console.log("edit book detail");
			$scope.editBook = bookEdit;
			$scope.editBook.genre = 5;
			console.log(bookEdit);
		})
		.run(function ($rootScope, $http, $location, $localStorage) {
			// keep user logged in after page refresh
			if ($localStorage.currentUser) {
				$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
			}
		})
		;