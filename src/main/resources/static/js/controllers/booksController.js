(function () {
	'use strict';
	angular
			.module('app')
			.controller('booksController', Controller);
	function Controller($location, $scope, $http, $localStorage, $rootScope) {
		initController();
		function initController() {
			$rootScope.errorShow = false;
			$rootScope.successShow = false;
		}
		;

		var paginationOptions = {
			pageNumber: 1,
			pageSize: 5,
			sort: null
		};

		$http({
			method: 'GET',
			url: '/api/v1/book/list?page=' + paginationOptions.pageNumber + '&size=' + paginationOptions.pageSize
		}).success(function (data) {
			console.log(data);
			$scope.gridOptions.data = data.content;
			$scope.gridOptions.totalItems = data.totalElements;
		});

		$scope.gridOptions = {
			paginationPageSizes: [5, 10, 20],
			paginationPageSize: paginationOptions.pageSize,
			enableColumnMenus: false,
			useExternalPagination: true,
			columnDefs: [
				{name: 'id', cellTemplate: '<div class="bookList ui-grid-cell-contents"><a ui-sref="bookDetail({id: {{row.entity.id}}})" \n\
									ui-sref-active="active" href="#">{{row.entity.id}}</a></div>'},
				{name: 'title', displayName: 'Název'},
				{name: 'author', displayName: 'Autor'},
				{name: 'genre.nameCz', displayName: 'Kategorie'},
				{name: 'isbn', displayName: 'ISBN'}
			],
			onRegisterApi: function (gridApi) {
				$scope.gridApi = gridApi;
				gridApi.pagination.on.paginationChanged(
						$scope,
						function (newPage, pageSize) {
							paginationOptions.pageNumber = newPage;
							paginationOptions.pageSize = pageSize;
							$http({
								method: 'GET',
								url: '/api/v1/book/list?page=' + paginationOptions.pageNumber + '&size=' + paginationOptions.pageSize
							}).success(function (data) {
								$scope.gridOptions.data = data.content;
								$scope.gridOptions.totalItems = data.totalElements;
							});
						});
			}
		}
	}
})();