(function () {
	'use strict';

	angular
			.module('app')
			.controller('bookController', Controller);

	function Controller($location, $scope, $http, $localStorage, bookEdit, bookDetail, $rootScope) {
		console.log("books controller init");
		initController();

		function initController() {
			$scope.newBook = {};
			$scope.genresList = {};
			if (bookEdit !== undefined) {
				$scope.editBook = bookEdit;
			}
			$scope.bookDetail = bookDetail;

			$http({
				method: 'GET',
				url: '/api/v1/book/genres'
			}).then(function successCallback(response) {
				console.log(response);
				$scope.genresList = response.data.genreType;

			}, function errorCallback(response) {
				$scope.errorShow = true;
				$scope.errorMsg = "Nelze načíst seznam kategorií.";

			});
			$rootScope.errorShow = false;
			$rootScope.successShow = false;
		}
		;

		$scope.newBook = function () {
			if ($localStorage.currentUser) {
				$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
			}
			var userData = new Object();
			console.log("new book controller");
			console.log($scope.newBook.genre);
			console.log($scope.newBook.author);
			userData.title = $scope.newBook.title;
			userData.description = $scope.newBook.description;
			userData.author = $scope.newBook.author;
			userData.isbn = $scope.newBook.isbn;
			userData.genreId = $scope.newBook.genre.id;
			console.log(userData);
			$http({
				method: 'POST',
				url: '/api/v1/book/new',
				data: userData
			}).then(function successCallback(response) {
				$rootScope.successShow = true;
				$rootScope.successMsg = response.data.message;
			}, function errorCallback(response) {
				$scope.$rootScope = true;
				$scope.$rootScope = response.data.message;

			});
		}

		$scope.updateBook = function () {
			if ($localStorage.currentUser) {
				$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
			}
			console.log("edit book controller");
			console.log($scope.editBook.genre);
			console.log($scope.editBook.author);
			var userData = new Object();
			userData.id = $scope.editBook.id;
			userData.title = $scope.editBook.title;
			userData.description = $scope.editBook.description;
			userData.author = $scope.editBook.author;
			userData.isbn = $scope.editBook.isbn;
			userData.genreId = $scope.editBook.genre.id;
			console.log(userData);
			$http({
				method: 'PUT',
				url: '/api/v1/book',
				data: userData
			}).then(function successCallback(response) {
				console.log(response);
				$rootScope.successShow = true;
				$rootScope.successMsg = response.data.message;
			}, function errorCallback(response) {
				$rootScope.errorShow = true;
				$rootScope.errorMsg = response.data.message;

			});
		}
	}
})();