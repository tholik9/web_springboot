# Project name: web_springboot
- Autor: Tomas Holik
- Project: UTB FAI, AYTWW, Zlin 2018

Application to show basic management of books.
Application have two parts:
### 1. Backend (server)
- in spring boot
- RESTful API
- JPA with MYSQL db
- maven used as dependency manager
- for authentication is used JWT(Javascript Web Token)
- property file web_springboot\src\main\resources\application.properties

### 2.Frontend (client)
- Angular v1.4
- maven used as dependency manager
- used ui-grid for pagination on books
- runs on http://localhost:8080

## How to run application

- To run application you need to set correct database in property file. 
- By default it will try to connect to local database: mysql://localhost:3306/webSpring
- Its enough just to create database without any tables (during startup all missing db entities will be created based by JPA mapping)
- To create .jar run "mvn package"
- To run application "mvn spring-boot:run"
- http://localhost:8080

